# README #

### What to use? ###
※疑似的なコードです

```
#!c++

//Initialize analyzer
SpectrumAnalyzer a;
a.initialize();
a.CreateBuffer( 2048, 1024 );
a.SetWindowFunc( WINDOWFUNC_HAMMING );

//Allocate band definitions you need.
a.ReserveBands( 1 );

//setup range of each bands
Bad& band = a.GetBand(0);
band.minFreq = xxxx;
band.maxFreq = yyyy;

//
while(true)
{
	//add audio samples
	for( i = 0 ; i < numCapturedSamples ; ++i )
	{
		a.AddSample( capturedSamples[i] );
	}
	
	//calculate the spectrum
	a.Update();
	
	//
	for( int i = 0 i < a.GetandCount() ; ++i )
	{
		printf( "Band#%d power=%f, power in dB=%f\n", a.GetBand().power, a.Getband().powerDb );
	}
}


```

### Notes

- SpectrumAnalyzerのSamplingRateはデフォルトで44.1ｋHですが、変更するMethodがないので必要だったら追加してください