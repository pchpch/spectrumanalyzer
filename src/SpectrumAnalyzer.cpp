#include "stdafx.h"
#include "SpectrumAnalyzer.h"

#include "fftsg_h.h"

const float Band::MIN_DB	= -96.0f;

SpectrumAnalyzer::SpectrumAnalyzer()
: m_SampleRate( 44100.0f ), m_WritePtr(0), m_vecBands( 0 ), m_Buffer(), m_ProcessBuffer(), m_WindowFunc()
{
}

SpectrumAnalyzer::~SpectrumAnalyzer()
{
}

bool	SpectrumAnalyzer::Initialize()
{
	return true;
}

void	SpectrumAnalyzer::CreateBufffer( const int bufferSize, const int windowSize )
{
	m_Buffer.Create( bufferSize, 0, 0 );
	m_Buffer.ZeroClear();
	m_WritePtr = 0;
	m_ProcessBuffer.Create( windowSize, 0, 0 );
	m_WindowFunc.Create( windowSize, 0, 0 );
	SetWindowFunc( WINDOWFUNC_HAMMING );
}

bool	SpectrumAnalyzer::Reset()
{
	m_Buffer.ZeroClear();
	m_WritePtr = 0;
	return true;
}

void	SpectrumAnalyzer::Update()
{
	FillProcessBuffer();
	CalcSpectrum();
	UpdatePowerOfAllBands();
}

//
void	SpectrumAnalyzer::FillProcessBuffer()
{
	int ptr = m_WritePtr - m_ProcessBuffer.NumElements();
	int copied = 0;
	if( ptr < 0 )
	{
		memcpy( m_ProcessBuffer.pBuf, ( m_Buffer.pBuf + m_Buffer.NumElements() + ptr ), -ptr * m_ProcessBuffer.GetElementSize() );
		copied -= ptr;
		ptr = 0;
	}

	const int rest = m_ProcessBuffer.NumElements() - copied;
	if( rest )
	{
		memcpy( m_ProcessBuffer.pBuf + copied, m_Buffer.pBuf + ptr, rest * m_ProcessBuffer.GetElementSize() );
	}
}


void	SpectrumAnalyzer::CalcSpectrum()
{
	for( int i = 0 ; i < m_ProcessBuffer.NumElements() ; ++i )
	{
		m_ProcessBuffer.pBuf[i] *= m_WindowFunc.pBuf[i];
	}

	rdft( m_ProcessBuffer.NumElements(), 1, m_ProcessBuffer.pBuf );
}


int		SpectrumAnalyzer::SpectrumIndexFromFrequency( const float frequency ) const
{
	const float wndLenInSec = (float)m_ProcessBuffer.NumElements() / m_SampleRate;

	int idx = min( (int)( frequency * wndLenInSec ), m_ProcessBuffer.NumElements() );
	if( idx >= m_ProcessBuffer.NumElements() /2 )
		idx = m_ProcessBuffer.NumElements() /2 - 1;
	return idx;
}

void	SpectrumAnalyzer::UpdatePowerOfAllBands()
{
	for( unsigned int i = 0 ; i < m_vecBands.size() ; ++i )
	{
		UpdateBandPower( m_vecBands[i] );
	}
}

void	SpectrumAnalyzer::UpdateBandPower( Band &band )
{
	float powMax = 0.0f;

	if( band.minFreq < band.maxFreq )
	{
		const int minIdx = SpectrumIndexFromFrequency( band.minFreq );
		const int maxIdx = SpectrumIndexFromFrequency( band.maxFreq );

		for( int i = minIdx ; i <= maxIdx ; ++i )
		{
			const float pow = sqrtf( Square( m_ProcessBuffer.pBuf[i*2] ) + Square( m_ProcessBuffer.pBuf[i*2+1] ) );
			if( pow > powMax )
			{
				powMax = pow;
			}
		}

		powMax /= m_ProcessBuffer.NumElements();
		powMax = Clamp( powMax, 1.0f/65535.0f, 1.0f );
	}

	band.power = powMax;

	if( powMax )
	{
		band.powerDb = max( band.MIN_DB, 20.f * log10(powMax) );
	}
	else
	{
		band.powerDb = band.MIN_DB;
	}
}


void	SpectrumAnalyzer::SetWindowFunc( const WINDOWFUNC_ID funcType )
{
	const unsigned int n = m_WindowFunc.NumElements();

	switch( funcType )
	{
	case WINDOWFUNC_HANNING:
		for( unsigned int i = 0 ; i < n ; ++i )
		{
			m_WindowFunc.pBuf[i] = 0.5f - 0.5f * cosf( 2 * PI * (float)i / (float)(n-1) );
		}
		break;

	case WINDOWFUNC_HAMMING:
		for( unsigned int i = 0 ; i < n ; ++i )
		{
			m_WindowFunc.pBuf[i] = 0.54f - 0.46f * cosf( 2 * PI * (float)i / (float)(n-1) );
		}
		break;

	case WINDOWFUNC_GAUSS:
		break;

	case WINDOWFUNC_BLACKMAN:
		break;
	}
}


