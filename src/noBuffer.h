#pragma once
/******************************************************************************

	noBuffer.h

	2005/08/xx	1st	Norihide Okada

******************************************************************************/

//-----------------------------------------------------------------------------
//
//	noBuffer
//
//-----------------------------------------------------------------------------
template<class TYPE>
struct noBuffer
{
public:
	TYPE *		pBuf;
	int			sizeInBytes;
	
	//
	noBuffer();
	~noBuffer();

	void Create( const size_t bufSize, const int align, const size_t ext );

	void ZeroClear();
	void Destroy();
	
	//
	int	NumElements() const;
	int GetElementSize() const;
};

///////////////////////////////////////////////////////////////////////////////
//
//
//
///////////////////////////////////////////////////////////////////////////////
template<class TYPE>
noBuffer<TYPE>::noBuffer():
pBuf(0), sizeInBytes(0)
{
}

template<class TYPE>
noBuffer<TYPE>::~noBuffer()
{
	Destroy();
}

template<class TYPE>
void noBuffer<TYPE>::Create( const size_t bufSize, const int align, const size_t ext )
{
	if( sizeInBytes != bufSize ) {
		Destroy();
		
		pBuf = (TYPE *)( new char[bufSize + ext] );		
		
		noAssert( pBuf );
		sizeInBytes = bufSize;
	}
}

template<class TYPE>
void noBuffer<TYPE>::Destroy()
{
	if( pBuf ) {
		delete [] pBuf;
		pBuf = NULL;
		sizeInBytes = 0;
	}
}

template<class TYPE>
inline void noBuffer<TYPE>::ZeroClear()
{
	memset( pBuf, 0, sizeInBytes );
}

template<class TYPE>
inline int	noBuffer<TYPE>::NumElements() const
{
	return sizeInBytes / sizeof(TYPE);
}

template<class TYPE>
inline int	noBuffer<TYPE>::GetElementSize() const
{
	return sizeof(TYPE);
}

