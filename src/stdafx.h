// stdafx.h : 標準のシステム インクルード ファイルのインクルード ファイル、または
// 参照回数が多く、かつあまり変更されない、プロジェクト専用のインクルード ファイル
// を記述します。
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// Windows ヘッダーから使用されていない部分を除外します。
// Windows ヘッダー ファイル :
#include <windows.h>
// C ランタイム ヘッダー ファイル
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <assert.h>
#include <math.h>

// TODO: プログラムに必要な追加ヘッダーをここで参照してください。
#include <vector>
#include <string>
#include <algorithm>
#include <memory>

//#include <SDL/Include/SDL.h>
//#include <SDL/Include/SDL_syswm.h>


//---------------------------------------------------------------
#define	SharedPtr	std::shared_ptr
#define	WeakPtr		std::weak_ptr
#define noVector	std::vector
#define noString	std::string

typedef unsigned int		uInt;
typedef int					Int;
typedef	bool				Bool;
#define True				true	
#define False				false

//---------------------------------------------------------------
#define	noReport( ... )	

#ifdef	_DEBUG
#define noAssert( e )	assert( (e) )
#else	//ifdef	_DEBUG
#define noAssert( e )	
#endif	//ifdef	_DEBUG


//---------------------------------------------------------------
template<class TDST, class TSRC>
inline TDST SafeCast( TSRC p )
{
#ifdef	_DEBUG
	TDST pnew = dynamic_cast<TDST>( p );
#else	//ifdef	_DEBUG
	TDST pnew = static_cast<TDST>( p );
#endif	//ifdef	_DEBUG

	noAssert( pnew );
	return pnew;
}

template<class T>
inline SharedPtr<T>		CreateSharedPtr( T *ptr )
{
	SharedPtr<T> sharedPtr( ptr );
	return sharedPtr;
}

template<class T>
inline void SafeDelete( T &p )
{
	delete p;
	p = NULL;
}

template<class T>
inline void SafeRelease( T &p )
{
	if( p )
	{
		p->Release();
		p = NULL;
	}
}

//---------------------------------------------------------------
template<class T>
inline void ZeroMem( T &target )
{
	memset( &target, 0, sizeof(target) );
}

//---------------------------------------------------------------
#ifndef PI
#define PI		3.14159265358979323846264338327950288 
#define TWOPI	(PI*2.0)
#endif	//ifndef PI

template<class T>
T Pi()
{
	return (T)PI;
}

template<class T>
inline T TwoPi()
{
	return Pi<T>() * (T)2;
}

template<class T>
inline T Square( const T val )
{
	return val*val;
}

template<class T>
inline T Clamp( const T val, const T minVal, const T maxVal )
{
	return min( max( val, minVal ), maxVal );
}

template<class T>
T Mod( const T val, const T modulator );

template<>
inline float Mod( const float val, const float modulator )
{
	return fmodf( val, modulator );
}

template<>
inline double Mod( const double val, const double modulator )
{
	return fmod( val, modulator );
}


//---------------------------------------------------------------
#define Find	std::find
#define Sort	std::sort


template<class T>
bool IsValueContainedInVector( noVector<T> &array, const T &value )
{
	return Find( array.begin(), array.end(), value ) != array.end();
}

