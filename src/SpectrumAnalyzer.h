#pragma once

#include "noBuffer.h"


struct Band
{
	static const float	MIN_DB;

	float	minFreq;
	float	maxFreq;
	float	power;
	float	powerDb;

	//
	Band()
	: minFreq(0.0f), maxFreq(0.0f), power(0.0f), powerDb(MIN_DB)
	{}
};



class SpectrumAnalyzer
{
public:
	enum WINDOWFUNC_ID
	{
		WINDOWFUNC_HANNING,
		WINDOWFUNC_HAMMING,
		WINDOWFUNC_GAUSS,
		WINDOWFUNC_BLACKMAN,
	};

private:
	noBuffer<float>		m_Buffer;
	int					m_WritePtr;

	noBuffer<float>		m_ProcessBuffer;
	noBuffer<float>		m_WindowFunc;

	noVector<Band>		m_vecBands;

	float				m_SampleRate;

public:
	SpectrumAnalyzer();
	virtual ~SpectrumAnalyzer();

	bool	Initialize();
	void	CreateBufffer( const int bufferSize, const int windowSize );

	bool	Reset();
	void	Update();

	void			ReserveBands( const unsigned int count );
	unsigned int	GetBandCount() const;
	Band &			GetBand( const unsigned int index );


	void	AddSample( const float val );

	int		SpectrumIndexFromFrequency( const float frequency ) const;

	void	SetWindowFunc( const WINDOWFUNC_ID funcType );


private:
	void	FillProcessBuffer();
	void	CalcSpectrum();
	void	UpdatePowerOfAllBands();
	void	UpdateBandPower( Band &band );
};

//
inline 	void	SpectrumAnalyzer::AddSample( const float val )
{
	m_Buffer.pBuf[m_WritePtr] = val;
	m_WritePtr = ( m_WritePtr + 1 ) % m_Buffer.NumElements();
}

inline 	void SpectrumAnalyzer::ReserveBands( const unsigned int count )
{
	if( m_vecBands.size() < count )
	{
		m_vecBands.resize( count );
	}
}

inline 	unsigned int SpectrumAnalyzer::GetBandCount() const
{
	return m_vecBands.size();
}

inline 	Band & SpectrumAnalyzer::GetBand( const unsigned int index )
{
	return m_vecBands[index];
}

